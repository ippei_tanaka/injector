module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        express: {
            test: {
                options: {
                    port: 8080,
                    bases: __dirname
                }
            }
        },
        mocha: {
            options: {
                run: true
            },
            test: {
                src: ['test/**/*.html']
            }
        }
    });

    // Load grunt tasks
    grunt.loadNpmTasks('grunt-mocha');
    grunt.loadNpmTasks('grunt-express');

    grunt.registerTask('test', ['express', 'express-keepalive']);
    grunt.registerTask('default', ['test']);
};