(function (global) {
    'use strict';

    var Injector = function Injector() {
        this._components = {};
        this._factories = {};
    };

    Injector.prototype.run = function (factory) {
        var _factory = new Injector.Factory(factory);
        return this._runFactory(_factory);
    };

    Injector.prototype.register = function (name, factory) {
        if (this._factories[name]) {
            throw new Error('"' + name + '" has already been registered.');
        }
        this._factories[name] = new Injector.Factory(factory);
        return this;
    };

    Injector.prototype.get = function (name) {
        return this._getComponent(name);
    };

    Injector.prototype._getComponent = function (name) {
        if (!this._components[name]) {
            this._components[name] = this._createComponents(name);
        }
        return this._components[name];
    };

    Injector.prototype._createComponents = function (name) {
        var factory = this._factories[name];

        if (typeof factory === "undefined") {
            throw new Error('"' + name + '" hasn\'t been registered.');
        }

        return this._runFactory(factory);
    };

    Injector.prototype._runFactory = function (factory) {
        var Factory,
            dependencies,
            dependedComponents = [],
            i;

        Factory = factory.getFactory();
        dependencies = factory.getDependencies();

        if (dependencies.length > 0) {
            for (i = 0; i < dependencies.length; i++) {
                dependedComponents.push(this._getComponent(dependencies[i]));
            }
        }

        return Factory.apply(this, dependedComponents);
    };

    /*----------------------------------------------*/

    Injector.Factory = function (factory) {
        this._factory = null;
        this._dependencies = [];

        if (typeof factory === "function") {
            this._factory = factory;
        } else if (Object.prototype.toString.call(factory) === '[object Array]') {
            this._factory = factory.splice(-1, 1)[0];
            this._dependencies = factory;
        }
    };

    Injector.Factory.prototype.getFactory = function () {
        return this._factory;
    };

    Injector.Factory.prototype.getDependencies = function () {
        return this._dependencies;
    };

    /*----------------------------------------------*/

    global.Injector = Injector;

}(this));
