describe('Injector', function () {

    describe('Injector.Factory', function () {

        it('should return constructor(Function) and dependencies(Array)',
            function () {
                var Module = function () {},
                    factory;

                Module.prototype.sum = function (a, b) {
                    return a + b;
                };

                factory = new Injector.Factory(Module);

                expect(factory.getFactory()).to.be.an('function');
                expect(factory.getDependencies()).to.be.an('array');
            });

        it('should return constructor and dependencies whose length is 0',
            function () {
                var Module = function () {},
                    factory;

                Module.prototype.sum = function (a, b) {
                    return a + b;
                };

                factory = new Injector.Factory([Module]);

                expect(factory.getFactory()).to.be.an('function');
                expect(factory.getDependencies().length).to.equal(0);
            });

        it('should return constructor and dependencies whose length is 3',
            function () {
                var Module = function () {},
                    factory;

                Module.prototype.sum = function (a, b) {
                    return a + b;
                };

                factory = new Injector.Factory(["Service1", "Service2", "Service3", Module]);

                expect(factory.getFactory()).to.be.an('function');
                expect(factory.getDependencies().length).to.equal(3);
            });
    });

    describe('Injector', function () {

        it('should get "MyService" and run sum()',
            function () {
                var injector = new Injector(),
                    Module = function () {},
                    service;
                Module.prototype.sum = function (a, b) {
                    return a + b;
                };
                injector.register("MyService", function () { return new Module(); });
                service = injector.get("MyService");
                expect(service.sum(44, 56)).to.equal(100);
            });

        it('should run an anonymous function which depends on "MyModule"',
            function () {
                var injector = new Injector(),
                    Module = function () {};
                Module.prototype.sum = function (a, b) {
                    return a + b;
                };
                injector.register("MyModule", function () { return Module; });
                injector.run(["MyModule", function (Module) {
                    expect(new Module().sum(2, 2)).to.equal(4);
                }]);
            });

        it('should get "MyService" which has dependency on "TheirService"',
            function () {
                var injector = new Injector(),
                    MyService = function (TheirService) { this._TheirService = TheirService; },
                    TheirService = function () {},
                    myService,
                    theirService;

                TheirService.prototype.multiply = function (a, b) {
                    return a * b;
                };

                MyService.prototype.double = function (a) {
                    return this._TheirService.multiply(2, a);
                };

                injector
                    .register("TheirService", function () { return new TheirService() })
                    .register("MyService", ["TheirService", function (TheirService) { return new MyService(TheirService) }]);
                myService = injector.get("MyService");
                theirService = injector.get("TheirService");

                expect(myService.double(44)).to.equal(88);
                expect(theirService.multiply(2, 4)).to.equal(8);
            });

        it('should get "MyService" which has dependency on "TheirService", "HerService" and "HisService"',
            function () {
                var injector = new Injector(),
                    MyService = function (TheirService, HerService) {
                        this._TheirService = TheirService;
                        this._HerService = HerService;
                    },
                    TheirService = function (HisService) {
                        this._HisService = HisService;
                    },
                    HisService = function () {},
                    HerService = function () {
                        this._name = "No Name";
                    },
                    myService,
                    herService;


                MyService.prototype.greet = function () {
                    return this._TheirService.getModifiedMessage() + " " + this._HerService.getName() + "!";
                };

                TheirService.prototype.getModifiedMessage = function () {
                    return this._HisService.getMessage() + "!";
                };

                HisService.prototype.getMessage = function () {
                    return "Hey";
                };

                HerService.prototype.getName = function () {
                    return this._name;
                };

                HerService.prototype.setName = function (name) {
                    this._name = name;
                };

                injector
                    .register("TheirService", ["HisService", function (HisService) { return new TheirService(HisService); }])
                    .register("HisService", function () { return new HisService(); })
                    .register("MyService", ["TheirService", "HerService", function (TheirService, HerService) { return new MyService(TheirService, HerService); }])
                    .register("HerService", function () { return new HerService(); });

                myService = injector.get("MyService");
                herService = injector.get("HerService");

                expect(herService.getName()).to.equal("No Name");
                expect(myService.greet()).to.equal("Hey! No Name!");
                herService.setName("John");
                expect(myService.greet()).to.equal("Hey! John!");
            });

        it('should throw an error when trying to load a service which is not registered',
            function () {
                var injector = new Injector()
                injector.register("MyService", ["Unknown", function () {}]);
                expect(function () { injector.get("MyService"); }).to.throw('"Unknown" hasn\'t been registered.');
            });

        it('should throw an error when trying to register a duplicate service',
            function () {
                var injector = new Injector();
                injector.register("MyService", ["Unknown", function () {}]);
                expect(function () { injector.register("MyService", function () {}); }).to.throw('"MyService" has already been registered.');
            });

        it('should return the reference of the object when get() is called for the second time',
            function () {
                var injector = new Injector(),
                    Module = function () {},
                    module1,
                    module2;
                injector.register("MyService", function () { return new Module(); });
                module1 = injector.get("MyService");
                module2 = injector.get("MyService");
                expect(module1).to.equal(module2);
            });
    });

});
